public class Application {
	public static void main(String[] args) {
		Quokka dave = new Quokka();
		Quokka samir = new Quokka();
		
		dave.gender = "female";
		dave.diet = "herbivore";
		dave.name = "dave";
		dave.age = 2;
		
		samir.gender = "male";
		samir.diet = "herbivore";
		samir.name = "samir";
		samir.age = 9;
		/*
		dave.sayHi(dave.age, dave.gender, dave.name);
		samir.sayHi(samir.age, samir.gender, samir.name);
		dave.playBall(dave.name);
		samir.playBall(samir.name);
		*/
		Quokka[] shaka = new Quokka[3];
		shaka[0] = dave;
		shaka[1] = samir;
		
		shaka[2] = new Quokka();
		shaka[2].name = "Nathan";
		shaka[2].diet = "herbivore";
		shaka[2].age = 20;
		shaka[2].gender = "female";
		
		shaka[2].playBall(shaka[2].name);
	}
}