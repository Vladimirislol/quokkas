public class Quokka {
	public String gender;
	public String diet;
	public String name;
	public int age;
	public void sayHi(int age, String gender, String name) {
		System.out.println("Hi! my name is "+name+", and I am a "+age+" year old "+gender+" quokka.");
	}
	public void playBall(String name) {
		System.out.println("I'm "+name+" and I'm ballin'");
	}
}